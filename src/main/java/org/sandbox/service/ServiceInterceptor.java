package org.sandbox.service;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

@Provider
public class ServiceInterceptor implements ContainerRequestFilter,
		ContainerResponseFilter {

	@Context
	private ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext,
			ContainerResponseContext responseContext) throws IOException {

//		System.out.println(requestContext.getHeaders().get("someHeader").get(0));

	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

	}

}
