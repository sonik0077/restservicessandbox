/**
 * 
 */
package org.sandbox.service;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.sandbox.model.User;

/**
 * @author PLPALES
 *
 */

@Path("user")
public class UserInfoService {
	
	@GET
    @Path("{id}")
    @Produces("application/json")
    @Consumes("application/json")
	public User getUser(@PathParam("id") int id) {
		User user = new User();
		user.setId(id);
		user.setName("SomeName");
		user.setAge((int) (Math.random() * 100));
		return user;
	}
	
	@GET
    @Produces("application/json")
    @Consumes("application/json")
	public List<User> getUsers() {
		return Arrays.asList(new User(0, "Andrew", 23), new User(1, "Sally", 21));
	}
	
}
